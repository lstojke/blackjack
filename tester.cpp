#include <iostream>
#include <cassert>
#include <ctime>
#include "cSet.h"
#include "complexNumbers2.h"
#include "menu2.h"
#include "cards.h"
#include "blackjack.h"
#include "tester.h"

void tester::checkAmount()
{
    deck d1;
    d1.makeDeck();
    assert(d1.decks.size()==52);
}

void tester::checkClear()
{
    deck d1;
    d1.makeDeck();
    d1.clearDeck();
    assert(d1.isEmpty() == 1);
}

void tester::ComplexAdd()
{
    cNum a(3.5,-2);
    cSet<cNum> s1;
    s1.add(a);
    assert( a == s1.getPt(1)->data);
}

void tester::setRemove()
{
    card c;
    cSet<card> s1;
    s1.add(c);
    s1.rmove(c);
    assert(s1.size()==0);
}
