#include <iostream>
#include <ctime>
#include <cassert>
#include "cards.h"
#include "cSet.h"
using namespace std;
void deck::makeDeck()
{
    string color[4] = {"KIER","KARO","TREFL","PIK"};
    string name[13] = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    for(int i=0;i<4;i++)
    {
        for(int j=0;j<9;j++)
        {
            card temp(j+2,name[j],color[i]);
            decks.add(temp);
        }
        for(int j=9;j<12;j++)
        {
            card temp(j-7,name[j],color[i]);
            decks.add(temp);
        }
        card temp(11,name[12],color[i]);
        decks.add(temp);
    }
}
bool deck::isEmpty()
{
    if(decks.size()==0)
        return true;
    else
        return false;
}
card deck::takeCard()
{
    srand(time(NULL));
    assert(!isEmpty());
    int ind = rand()%decks.size();
    card temp = decks.getPt(ind)->data;
    decks.rmove(temp);
    return temp;
}

void deck::addCart(card c)
{
    decks.add(c);
}

int deck::showValue()
{
    int s=0;
    for(int i=1;i<=decks.size();i++)
    {
        s+=decks.getPt(i)->data.getValue();
    }
    return s;
}

void deck::clearDeck()
{
    while(decks.size()!=0)
    {
        card c = takeCard();
    }
}

bool card::operator==(const card c1)
{
    if(name==c1.name && color==c1.color)
        return true;
    else
        return false;
}

std::ostream & operator<< (std::ostream &out, const deck &d)
{
    out << d.decks;
    return out;
}
