#ifndef cards_H
#define cards_H

#include <iostream>
#include "cSet.h"
using namespace std;
class card
{
    int val; ///Wartosc karty w "oczku"
    string name; ///Figura
    string color; ///Kolor
public:
    card(int v=0, string n="2", string c="PIK"): val(v),name(n),color(c) {};
    int getValue() {return val;}
    string getName() {return name;}
    string getColor() {return color;}
    bool operator==(const card c1);
    friend ostream & operator<<(ostream& out, card &c)
    {
        out<<c.getName()<<" "<<c.getColor();
        return out;
    }
};

class deck
{
    cSet<card> decks;
public:
    void makeDeck(); ///Tworzy cala talie
    card takeCard(); ///Zabiera losowa karte z wybranej talii
    void addCart(card c); ///Dodaje karte do wybranej talii
    bool isEmpty(); ///Sprawdza, czy talia jest pusta
    void clearDeck(); ///Pozbywa sie wszystkich kart z talii
    int showValue();  ///Zwraca sume wartosci kart w talii
    template <class U>
    friend std::ostream & operator<< (std::ostream &out, const cSet<U> &s);
    friend std::ostream & operator<< (std::ostream &out, const deck &d);
    friend void blackjack();
    friend class tester;

};



#endif // cards_H
