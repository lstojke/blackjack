#include <iostream>
#include <assert.h>
#include "complexNumbers2.h"
#include "cards.h"
#include "cSet.h"
using namespace std;

template <class T>
T load()
{
    T x;
    cin >> x;
    while(!cin.good())
    {
        cout << "Blad! Wczytaj poprawna liczbe" << endl;
        cin.clear();
        cin.ignore(1000,'\n');
        cin >> x;
    }
    cin.clear();
    cin.ignore(1000,'\n');

    return x;
}
template int load();
template double load();
template cNum load();


template <class T>
element<T>::element(T a)
{
	data = a;
}
template element<int>::element(int a);
template element<double>::element(double a);
template element<cNum>::element(cNum a);
template element<card>::element(card a);


template <class T>
cSet<T>::cSet()
{
    head = NULL;
}
template cSet<int>::cSet();
template cSet<double>::cSet();
template cSet<cNum>::cSet();
template cSet<card>::cSet();

template <class T>
cSet<T>::~cSet()
{
	element<T> *current = head;
	element<T> *next1;
    while( current )
        {
			next1 = current->next;
            delete current;
			current = next1;
        }
	head = NULL;
	assert(head==NULL); ///TESTY
}
template cSet<int>::~cSet();
template cSet<double>::~cSet();
template cSet<cNum>::~cSet();
template cSet<card>::~cSet();

template <class T>
element<T>* cSet<T>::getPt(int n)const
{
    if(!head)
        return NULL;
    element<T> *temp = head;
	for(int i=0; i<n; ++i)
	{
		if( i == n-1)
        {
            assert(temp!=NULL);
			return temp;
        }
		else if(temp->next)
			temp = temp->next;
		else
            {
            cout<<"WYJSCIE POZA ROZMIAR ZBIORU!";
            exit(0);
            return NULL;
            }
	}
	return head;
}
template element<int>* cSet<int>::getPt(int n)const;
template element<double>* cSet<double>::getPt(int n)const;
template element<cNum>* cSet<cNum>::getPt(int n)const;
template element<card>* cSet<card>::getPt(int n)const;

template <class T>
int cSet<T>::size()
{
    int sum=0;
    element<T> *temp = head;
    while(temp)
    {
        sum++;
        temp = temp->next;
    }
    return sum;
}
template int cSet<int>::size();
template int cSet<double>::size();
template int cSet<cNum>::size();
template int cSet<card>::size();


template <class T>
void cSet<T>::add(T data)
{
 	element<T> *newNum = new element<T>(data);
 	assert(newNum != NULL); ///TESTY
 	newNum->next = NULL;
 	if(head == NULL)
 	{
 	    head = newNum;
		return;
 	}
 	else
	{
    	 element<T> *temp = head;
    	 while(temp)
    	{
    	    if(temp->data==newNum->data)
    	    {
    	        delete newNum;
    	        return;
    	    }
    	    else if(temp->next == NULL)
    	    {
    	        temp->next=newNum;
    	        return;
    	    }
    	    temp = temp->next;
    	}
	}
}
template void cSet<int>::add(int data);
template void cSet<double>::add(double data);
template void cSet<cNum>::add(cNum data);
template void cSet<card>::add(card data);

template <class T>
void cSet<T>::rmove(T x)
{
    if(!head)
    {
        return;
    }
    else
    {
        element<T> *temp = head;
		element<T> *prev = NULL;
        int i = 1;
        while (temp)
        {
            if(temp->data == x)
            {
                if(temp == head)
                    if(head->next)
                        head = head->next;
                    else
                        head = NULL;
                else
                    prev->next = temp->next;
                delete temp;
                return;
            }
			++i;
			prev = temp;
            temp = temp->next;
        }
    }
	return;
}
template void cSet<int>::rmove(int x);
template void cSet<double>::rmove(double x);
template void cSet<cNum>::rmove(cNum x);
template void cSet<card>::rmove(card x);

template <class T>
cSet<T> cSet<T>::operator +(const cSet<T> &s) const&
{
    cSet<T> sTemp;
    element<T> *temp = s.head;
    while (temp)
    {
        sTemp.add(temp->data);
        temp = temp->next;
    }
    temp = head;
    assert(temp != NULL);   ///TESTY
    while (temp)
    {
        sTemp.add(temp->data);
        temp = temp->next;
    }
    return sTemp;
}
template cSet<int> cSet<int>::operator +(const cSet<int> &s) const&;
template cSet<double> cSet<double>::operator +(const cSet<double> &s) const&;
template cSet<cNum> cSet<cNum>::operator +(const cSet<cNum> &s) const&;
template cSet<card> cSet<card>::operator +(const cSet<card> &s) const&;


template <class T>
cSet<T>& cSet<T>::operator +=(const cSet<T> &s)
{

    element<T> *temp = s.head;
    while (temp)
    {
        add(temp->data);
        temp = temp->next;
    }
    return *this;
}
template cSet<int>& cSet<int>::operator +=(const cSet<int> &s);
template cSet<double>& cSet<double>::operator +=(const cSet<double> &s);
template cSet<cNum>& cSet<cNum>::operator +=(const cSet<cNum> &s);
template cSet<card>& cSet<card>::operator +=(const cSet<card> &s);

template <class T>
cSet<T> cSet<T>::operator *(const cSet<T> &s) const&
{
    cSet<T> sTemp;
    element<T> *temp = s.head;

    while (temp)
    {
        element<T> *temp2 = head;
        while(temp2)
        {
            if(temp->data==temp2->data)
                sTemp.add(temp->data);
            temp2 = temp2->next;
        }
        temp = temp->next;
    }
    return sTemp;
}
template cSet<int> cSet<int>::operator *(const cSet<int> &s) const&;
template cSet<double> cSet<double>::operator *(const cSet<double> &s) const&;
template cSet<cNum> cSet<cNum>::operator *(const cSet<cNum> &s) const&;
template cSet<card> cSet<card>::operator *(const cSet<card> &s) const&;

template <class T>
cSet<T>& cSet<T>::operator *=(const cSet<T> &s)
{
    element<T> *temp = head;
    while (temp)
    {
		element<T> *temp2 = s.head;
        bool flag = false;
        while(temp2)
        {
            if(temp->data==temp2->data)
                flag = true;
            temp2 = temp2->next;
        }
		element<T> *help = temp;
		temp = temp->next;
        if(!flag)
            rmove(help->data);

    }
    return *this;
}
template cSet<int>& cSet<int>::operator *=(const cSet<int> &s);
template cSet<double>& cSet<double>::operator *=(const cSet<double> &s);
template cSet<cNum>& cSet<cNum>::operator *=(const cSet<cNum> &s);
template cSet<card>& cSet<card>::operator *=(const cSet<card> &s);


template <class T>
std::ostream & operator<< (std::ostream &out, const cSet<T> &s)
{
    //int i=1;
	element<T> *temp = s.head;

    if(s.head == NULL)
    {
        out << "Ten zbior jest pusty" << std::endl;
        return out;
    }
	while(temp != NULL) {
	    out << temp->data << " ";
	  	temp = temp->next;
	 	}
    return out;
}
template std::ostream & operator<< (std::ostream &out, const cSet<int> &s);
template std::ostream & operator<< (std::ostream &out, const cSet<double> &s);
template std::ostream & operator<< (std::ostream &out, const cSet<cNum> &s);
template std::ostream & operator<< (std::ostream &out, const cSet<card> &s);
