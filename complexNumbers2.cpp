#include <iostream>
#include "complexNumbers2.h"

cNum::cNum(double a, double b)
{
    x=a;
    y=b;
}
cNum cNum::operator+(const cNum n1)
{
    return cNum(x+n1.x, y+n1.y);
}
cNum cNum::operator+=(const cNum n1)
{
    x+=n1.x;
    y+=n1.y;
    return *this;
}
cNum cNum::operator-(const cNum n1)
{
    return cNum(x-n1.x, y-n1.y);
}
cNum cNum::operator-=(const cNum n1)
{
    x-=n1.x;
    y-=n1.y;
    return *this;
}
bool cNum::operator==(const cNum n)
{
    if(x==n.x && y==n.y)
        return true;
    else
        return false;
}
bool cNum::operator!=(const cNum n)
{
    if(x==n.x && y==n.y)
        return false;
    else
        return true;
}
std::ostream & operator<< (std::ostream &exit, const cNum n)
{
    if (n.x !=0)
    {
        exit << n.x << " ";
        if (n.y > 0)
            exit << "+ ";
        if (n.y !=0)
            {
            if (n.y!=1 && n.y!=-1)
                exit << n.y;
            else if (n.y==-1)
                exit << "- ";
            exit << "i";
            }
    }
    else if (n.y!=0)
    {
        exit << "    ";
        if (n.y !=0)
            {
            if (n.y!=1 && n.y!=-1)
                exit << n.y;
            else if (n.y==-1)
                exit << "-";
            exit << "i";
            }
    }
    else
        exit << "0";
    return exit;
}

std::istream & operator>> (std::istream &open, cNum &n)
{
    std::cout << "Podaj wartosc czesci rzeczywistej liczby : ";
    open >> n.x;
    std::cout << "Podaj wartosc czesci urojonej liczby : ";
    open >> n.y;
    return open;
}

