pro2: main.o menu2.o complexNumbers2.o cSet.o cards.o blackjack.o tester.o
	g++ -o pro2 main.o menu2.o complexNumbers2.o cSet.o cards.o blackjack.o tester.o

main.o: main.cpp menu2.cpp complexNumbers2.cpp cSet.cpp cards.cpp blackjack.cpp
	g++ -c main.cpp menu2.cpp complexNumbers2.cpp cSet.cpp cards.cpp blackjack.cpp

menu2.o: menu2.h complexNumbers2.cpp menu2.cpp cSet.cpp
	g++ -c menu2.cpp complexNumbers2.cpp cSet.cpp

tester.h: menu2.cpp complexNumbers2.cpp cSet.cpp cards.cpp blackjack.cpp
	g++ -c menu2.cpp complexNumbers2.cpp cSet.cpp cards.cpp blackjack.cpp

cSet.o: cSet.h cSet.cpp complexNumbers2.cpp cards.cpp
	g++ -c cSet.cpp complexNumbers2.cpp cards.cpp

blackjack.o: blackjack.h blackjack.cpp cards.cpp
	g++ -c blackjack.cpp cards.cpp

cards.o: cards.h cards.cpp cSet.cpp
	g++ -c cards.cpp cSet.cpp

complexNumbers2.o: complexNumbers2.h complexNumbers2.cpp
	g++ -c complexNumbers2.cpp
