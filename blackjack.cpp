#include <iostream>
#include "blackjack.h"
#include "cSet.h"
#include "cards.h"
#include <ctime>

void start(deck *croupier, deck *player, deck *player2)
{
    player->clearDeck();
    player2->clearDeck();
    int r = rand()%4+2;
    croupier->makeDeck();
    for(int i=1;i<=2;i++)
    {
        card c = croupier->takeCard();
        player->addCart(c);
    }
    for(int i=1;i<=r;i++)
    {
        card c = croupier->takeCard();
        player2->addCart(c);
    }
}
int goAway(int choice)
{
    choice = 0;
    cout << "Chcesz zagrac jeszcze raz? (T/N)? ";
    char o;
    cin >> o;
    while (o!='T' && o!='t' && o!='N' && o!='n')
    {
        cout << endl << "Wpisz T, aby zagrac lub N, aby wyjsc ";
        cin.clear();
        cin.ignore(1000,'\n');
        cin >> o;
    }
    if(o == 't' || o == 'T')
        choice = -1;
    return choice;
}

void blackjack()
{
    int choice = -1;
    deck croupier;
    deck player;
    deck player2;
    do
    {
        /*card c(11,"A","KIER");
        player.addCart(c);
        card d(11,"A","PIK");
        player.addCart(d);
        choice = -2;*/
        if(choice == -1)
            start(&croupier,&player,&player2);
        system("clear");
        cout << "Twoje karty :\n" << player;
        cout << endl << "Wartosc Twoich kart : " << player.showValue() << endl;
        if(player.showValue()==22 && player.decks.size()==2)
        {
            cout << "Perskie oczko ! \n Wygrales !" << endl;
            choice = 0;
        }
        else if(player.showValue()>21 )
        {
            cout << "Za duza wartosc kart - PORAŻKA" << endl;
            choice = 0;
        }
        else
        {
            cout << "1 - Dobierz karte" << endl;
            cout << "2 - Sprawdz, czy wygrales" << endl;
            cout << "0 - Koniec gry" << endl;
            choice = load<int>();
        }
        if(choice == 1)
        {
            card c = croupier.takeCard();
            player.addCart(c);
        }
        else if(choice == 2)
        {
            cout << "Twoje karty : " << player << "\n\n\t\tvs.\n\nKarty rywala : " << player2;
            cout << endl << "\nWartosc Twoich kart vs. rywala : " << player.showValue();
            cout << " vs. " << player2.showValue() << endl;
            if(player.showValue() < player2.showValue() && player2.showValue() <= 21)
            {
                cout << endl << "PRZEGRALES :( " << endl;
            }
            else
            {
                cout << endl << "WYGRALES !" << endl;
            }
            choice = goAway(choice);
        }
        else
            if(choice == 0)
            choice = goAway(choice);
    }while(choice != 0);
}
